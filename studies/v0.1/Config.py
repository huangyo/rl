config = {
    "hid_size": 512,
    "lr": 0.0003,
    "batch_size": 64,
    "buffer_size": 500000,
    "tau": 0.005,
    "gamma": 0.995,
    "warm_up": 250000,
    "update_every": 50,
    "num_episodes": 600000,
    "eps": 0,
    "auto": False,
    "alpha": 0.01,
    "time_scale": 3.0
}

