import utils

from gym_unity.envs import UnityToGymWrapper
import lime
import lime.lime_tabular
import numpy as np
import torch

from sac.agent import SAC


class Explainer:
    '''
    @author: Adrian Kruger

    Can be used to local model explanations with LIME.
    See: https://github.com/marcotcr/lime

    :param model: The pytorch model
    '''

    def __init__(self, model):
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.env = UnityToGymWrapper(model.env, allow_multiple_obs=True)
        self.explainer = self.prepare(model)

    def prepare(self, model, n=100):
        #Lime explainer needs to estimante mean, std and other information about observation variables
        states = []
        state = torch.tensor(self.env.reset()[0], dtype=torch.float).to(self.device)
        print("Creating base dataset...")

        for _ in range(n):
            states.append(state.cpu().numpy())
            action = model.get_action(state)
            next_state, reward, done, info = self.env.step(action)
            if done:
                next_state = self.env.reset()
            state = torch.tensor(next_state[0], dtype=torch.float).to(self.device)

        states = np.array(states)
        explainer = lime.lime_tabular.LimeTabularExplainer(states, feature_names=[str(i) for i in range(172)],
                                                           mode="regression")
        return explainer

    def explain(self, state):
        def predict(states):
            actions = []
            for state in states:
                state = torch.tensor(state, dtype=torch.float).to(self.device)
                action = model.get_action(state)
                actions.append(action)
            actions = np.array(actions)
            return actions

        exp = self.explainer.explain_instance(state, predict, num_samples=100, num_features=8)
        return exp


def explain(model, frames_between_plot=10):
    '''
    @author: Adrian Kruger

    Create local explanation for every frames_between_plot frames

    :param model: The pytorch model
    '''
    explainer = Explainer(model)
    state = explainer.env.reset()[0]
    frame = 0
    while True:
        frame += 1
        action = model.get_action(torch.tensor(state).to(explainer.device))
        if frame % frames_between_plot == 0:
            exp = explainer.explain(state)
            print(exp.as_map())
            fig = exp.as_pyplot_figure()
            fig.show()
            input("Hit enter for next frame")
        next_state, reward, done, info = explainer.env.step(action)

        if done:
            next_state = explainer.env.reset()
        state = next_state[0]


if __name__ == "__main__":
    print("IMPORTANT: You need to delete autoloading weights in sac/agent __init__ first!.")
    env = utils.get_crawler_env(multi=False)
    config = {
        "hid_dims": [512, 512],  # must stay consistent with the model
        "max_steps": 10000,
        "time_scale": 1.0,
    }
    model = SAC(env, config, test=True)
    model.load("studies/v0.2")
    explain(model, frames_between_plot=10)
