import joblib
import json
import os
import time
import itertools

import optuna

from utils import get_crawler_env
from sac.agent import SAC

save_step = 50000  # save model every save_step


class Objective(object):
    '''
    @author: Adrian Kruger

    Used in conjunction with optuna to finetune hyperparameters on simultaneous runs
    '''

    def __init__(self):
        self.worker_counter = itertools.count()  # Unique worker_id for parallel training

    def __call__(self, trial, PATH):
        worker_id = next(self.worker_counter)
        # Modify default config
        config = {
            "rbed": True,
            "hid_dims": [512, 512],
            "lr": trial.suggest_float("lr", 0.0001, 0.001),
            "batch_size": 64,
            "buffer_size": 100000,
            "tau": trial.suggest_float("tau", 0.001, 0.01),
            "gamma": 0.995,
            "warm_up": 50000,
            "update_every": 60,
            "max_steps": 1000000,
            "eps": 0,
            "auto": False,
            "alpha": trial.suggest_float("alpha", 0.005, 0.2),
            "time_scale": 2.0,
            "eps_max": trial.suggest_float("eps_max", 0.005, 0.2),
            "eps_min": 0.001,
            "eps_decay": 0.99,
            "decay_val": 230,
            "r_target": 2500,
            "r_threshold": -1.0,
            "test": False,
        }

        # Create direcory for storing model weights and tensorboard
        run_directory = PATH + "/" + str(worker_id) + "_alpha" + str(config["alpha"])[0:6]
        os.mkdir(run_directory)
        with open(run_directory + "/config.json", 'w+') as f:
            json.dump(config, f, indent=4)

        # Create agent
        env = get_crawler_env(worker_id=worker_id, time_scale=config["time_scale"])
        agent = SAC(env, config, log_dir=run_directory)

        # Run study
        print("Start study with alpha {}".format(config["alpha"]))
        print("Warming up...")
        for i in range(config["max_steps"]):
            if i % 500 == 0 and i >= config["warm_up"]:
                print("Avg score of agent with alpha {}: {}".format(
                    round(config["alpha"], 4),
                    round(sum(agent.scores) / len(agent.scores), 5))
                )
                if i % save_step == 0:
                    save_dir = run_directory + "/" + str(i)
                    os.mkdir(save_dir)
                    agent.save(save_dir)
                    print("Agent with alpha {}: avg score = {}  best score = {}".format(
                        round(config["alpha"], 3),
                        round(sum(agent.scores) / len(agent.scores), 3),
                        round(max(max(agent.scores), agent.best_score), 3)))
            _ = agent.step() # this prevents prints from the step() function
        env.close()
        return sum(agent.scores) / len(agent.scores)


def run_study(studyname, n_trials=24, n_jobs=6):
    '''
    @author: Adrian Kruger

    Run a study and save study file

    :param n_trials: Totol number of trials
    :param n_jobs: Number of simultaneous trials in each run
    '''
    study_dir = "studies/" + str(studyname)
    os.mkdir(study_dir)

    study = optuna.create_study(direction='maximize', study_name=studyname)
    objective = Objective()

    runs = int(n_trials / n_jobs)

    for run in range(runs):
        # Split the runs so that we can save current study file in-between
        study.optimize(lambda trial: objective(trial, study_dir), n_trials=n_jobs, n_jobs=n_jobs)
        time.sleep(5)
        joblib.dump(study, study_dir + "/study.pkl")

    print("##### FINISHED All RUNS ######")


if __name__ == "__main__":
    id = "1_study"
    run_study(id, n_trials=20, n_jobs=5)

    # Load trials and results
    study = joblib.load("studies/" + str(id) + "/study.pkl")
    print("Trials:")
    print(study.get_trials())
    print("Best Trial:")
    print(study.best_trial)
