import json
import pathlib
import sys
from sys import platform

from mlagents_envs.environment import UnityEnvironment
from mlagents_envs.side_channel.engine_configuration_channel import EngineConfigurationChannel


def get_platform():
    '''
    @author: Adrian Kruger
    Get platform identifier string

    :return: "win32" if windows, "linux" if linux
    '''
    return platform


def get_crawler_env(worker_id=0, time_scale=1.0, headless=False, multi=True):
    '''
    @author: Adrian Kruger
    Load a crawler enviroment for training. New env paths should be logged here

    :param worker_id: unique worker id for thread
    :param time_scale: scale of env speed
    :param headless: for headless envs
    :param multi: for multi agent simultaneous training (currently 10 agents)
    :return: Unity env
    '''

    plat = get_platform()
    engine_config_channel = EngineConfigurationChannel()

    p = pathlib.Path(__file__).resolve().parent

    if not multi:
        env_path = p / "envs" / "crawler_linux" / "crawler.x86_64"
    elif plat == "win32":
        if headless:
            env_path = p / "envs" / "crawler_multi_win" / "UnityEnvironment.exe"
        else:
            raise NotImplementedError("No headless env for Windows")
    elif plat == "linux":
        if headless:
            env_path = p / "envs" / "crawler_multi_linux_server" / "crawlerServer.x86_64"
        else:
            env_path = p / "envs" / "crawler_multi" / "crawler.x86_64"
    else:
        raise NotImplementedError("No environment found for platform {}".format(platform))

    env_path_string = str(env_path.absolute())
    print("Loading env: {}".format(env_path_string))
    env = UnityEnvironment(env_path_string, worker_id=worker_id,
                           side_channels=[engine_config_channel], seed=0)
    engine_config_channel.set_configuration_parameters(time_scale=time_scale)
    env.reset()

    return env


def create_random_buffer(env, n=100, path="buffer/crawler/"):
    '''
    @author: Adrian Kruger

    Save n random steps of env as json
    TODO: Works for single agent env only

    :return: true, if success
    '''
    episode = 0
    buffer = {}
    states = env.reset()

    print("Creating buffer of size {}".format(n))
    for i in range(n):
        sys.stdout.write("\rStep: %i" % i)
        sys.stdout.flush()
        action = env.action_space.sample()
        next_states, reward, done, _ = env.step(action)

        buffer[str(i)] = {
            "state": [state.tolist() for state in states],
            "action": action.tolist(),
            "next_state": [next_state.tolist() for next_state in next_states],
            "reward": float(reward),
            "done": done,
            "episode": episode
        }

        if done:
            states = env.reset()
            episode += 1
        else:
            states = next_states

    with open(path+str(n)+".json", "w") as f:
        json.dump(buffer, f)

    return True


if __name__ == "__main__":
    test_env = get_crawler_env(worker_id=0, time_scale=4.0)
    test_env.close()
