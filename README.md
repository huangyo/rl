# Gruppe 7 (crawler)

## Getting Started


#### Setup the environment
###### _What do you need to run the project?:_
_First make sure you have installed **Python3**_
```
pip install mlagents==0.22.0
pip install mlagents-envs==0.22.0
```

#### <br>Train
_Open your terminal/cmd & switch to the **sac** directory and run following lines:_
```
python main.py PATH/TO/CONFIG/FILE
```
_The config files for training with or without reward-based-epsilon-decay (rbed) strategy and for testing are stored under the **configs** directory.
The log files for tensorboard will be stored under the **runs** directory under **sac**._  
_On linux platform, a Server-Build-Environment without rendering will be used for training. If the environment in our repository does not work, delete the **crawler_multi_linux_server** directory in **envs**, download the .zip file
 [here](https://drive.google.com/u/0/uc?export=download&confirm=TFOF&id=1JB5HNr8y9ZD8PTcgUHLIqS9-CWXaKfxM) and unzip it in the **envs** directory without changing the file name._


##### Create random buffer
_If you want to store random experiences for reuse in training, open your terminal/cmd & switch to the **sac** directory and run following lines:_

```
python main.py PATH/TO/CONFIG/FILE -b
```
_This will generate a file **random_buffer** that contains random experiences. The number of experiences collected corresponds to the
number of steps taken during warm-up phase specified in the config file._

##### Tune
_Open your terminal/cmd & switch to the project root directory and run following lines:_
```
python tune.py
```
_This will do automatic hyperparameter tuning.<br><br>_


#### Test / Demo
_Open your terminal/cmd & switch to the **sac** directory and run following lines:_

###### _Standard environment:_
```
python main.py configs/config_test.yaml -t
```

###### _Environment with uneven ground:_
```
python main.py configs/config_test.yaml -t -g
```

###### _Environment with static obstacles:_
```
python main.py configs/config_test.yaml -t -o
```

###### _Environment with missing right-front-leg:_
```
python main.py configs/config_test.yaml -t -l
```
_All of these commands will run the model stored in **studies/v0.3**, which has reached an average score of 1861 while training._

#### Explanations
WIP: Run python explain.py to get local explanations of a trained model. You need to delete auto-weigths loading in sac/agent.py l.33 first.



## Crawler environment
##### Set-up
A creature with 4 arms and 4 forearms. 
##### Goal  
The agents must move its body toward the goal without falling. The goals position in the environment is randomized.      
##### Agents    
The environment contains 10 agents with same Behavior Parameters.    
##### Agent Reward Function (independent):   
The reward function is geometric meaning the reward each step is a product of all the rewards instead of a sum.  
* Body velocity matches goal velocity. (normalized between (0,1))  
* Head direction alignment with goal direction. (normalized between (0,1))    
##### Behavior Parameters:  
* Vector Observation space: 172 variables corresponding to position, rotation, velocity, and angular velocities of each limb plus the acceleration and angular acceleration of the body.  
* Actions: 20 continuous actions, corresponding to target rotations for joints.  
##### Benchmark  
Benchmark Mean Reward for CrawlerDynamicTarget: 2000 <br><br> 

## Authors

##### Dominik Beyer
* Connection to environment with multiple agents
* Random_buffer generation
* Buffer initialization
* Research
* Training
* Testing / Experiments
* A2C-modell adjustments ( At the beginning our team used A2C as the rl-algorithm of choice)
* Testing with simpler (less complex) environments (Pendulum, Continuous MountainCar, BiPedalWalker)


##### Yongxin Huang
* SAC implementation
* Experiments without rbed strategy on Crawler(single/multi) and 3DBall(single)
* Code refactoring / project structure
* Research on actor-critic algorithms
* Implementation of A2C-model with entropy term (At the beginning our team used A2C as the rl-algorithm of choice)

##### Svenja König
* Implementation / Testing of different exploration-methods (epsilon-based strategies, noise on action- / parameter space), due to lack of exploration.
* Construct different versions of 'Crawler'-environment
* Testing / Experiments
* Training
* Research
* A2C-modell adjustments ( At the beginning our team used A2C as the rl-algorithm of choice)
* Testing with simpler (less complex) environments (Pendulum, Continuous MountainCar, BiPedalWalker)


##### Adrian Krüger
* Hyperparameter tuning (Optuna), 
* Explanations (Lime), 
* (Parallel) training
* Project structure/code refactoring
* Research
* Testing / Experiments
* A2C-modell adjustments ( At the beginning our team used A2C as the rl-algorithm of choice)
* Testing with simpler (less complex) environments (Pendulum, Continuous MountainCar, BiPedalWalker)
