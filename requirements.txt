mlagents==0.22.0
mlagents-envs==0.22.0
optuna
torch
numpy

# Optional for explanations
# lime
# gym-unity==0.22.0
