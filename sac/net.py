"""@author: Yongxin Huang"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions.normal import Normal
import numpy as np


class QNet(nn.Module):
    def __init__(self, obs_dim, act_dim, hid_dims):
        super(QNet, self).__init__()
        sequential = [nn.Linear(obs_dim + act_dim, hid_dims[0]), nn.LeakyReLU()]
        for i in range(0, len(hid_dims)-1):
            sequential.append(nn.Linear(hid_dims[i], hid_dims[i+1]))
            sequential.append(nn.LeakyReLU())
        self.fc = nn.Sequential(*sequential)
        self.q = nn.Linear(hid_dims[-1], 1)

    def forward(self, state, action):
        """Return Q value given state and action"""
        x = torch.cat((state, action), dim=-1)
        return self.q(self.fc(x))


LOG_STD_MAX = 2
LOG_STD_MIN = -20


class Actor(nn.Module):
    def __init__(self, obs_dim, act_dim, hid_dims):
        super(Actor, self).__init__()
        sequential = [nn.Linear(obs_dim, hid_dims[0]), nn.LeakyReLU()]
        for i in range(0, len(hid_dims)-1):
            sequential.append(nn.Linear(hid_dims[i], hid_dims[i+1]))
            sequential.append(nn.LeakyReLU())
        self.fc = nn.Sequential(*sequential)
        self.mu = nn.Linear(hid_dims[-1], act_dim)
        self.log_std = nn.Linear(hid_dims[-1], act_dim)

    def forward(self, state):
        """Return action sampled from Gaussian Distribution and its log probability."""
        mu = self.mu(self.fc(state))
        log_std = self.log_std(self.fc(state))
        log_std = torch.clamp(log_std, LOG_STD_MIN, LOG_STD_MAX)
        std = torch.exp(log_std)

        distribution = Normal(mu, std)
        action = distribution.rsample()

        log_prob = distribution.log_prob(action).sum(axis=-1, keepdim=True)
        # correction of log_prob of action after tanh squashing
        log_prob = log_prob - (2*(np.log(2) - action - F.softplus(-2*action))).sum(axis=-1, keepdim=True)

        action = torch.tanh(action)

        return action.squeeze(), log_prob

    def test(self, state):
        """Return tanh squashed mean as predicted action while inference"""
        return torch.tanh(self.mu(self.fc(state)).squeeze())

