"""@author: Yongxin Huang"""

import random
import torch
from collections import deque


class ReplayBuffer(object):
    """Replay buffer to store past experiences that the agent can then use for training data"""

    def __init__(self, buffer_size, device, seed):
        self.memory = deque(maxlen=buffer_size)
        self.device = device
        random.seed(seed)

    def add_experience(self, state, action, reward, next_state, done):
        """Adds experience into the replay buffer"""
        experience = map(lambda x: torch.as_tensor(x, dtype=torch.float), [state, action, reward, next_state, done])
        self.memory.append(tuple(experience))

    def sample(self, num_experiences):
        """Draws random samples from the replay buffer"""
        experiences = random.sample(self.memory, k=num_experiences)
        states = torch.vstack([e[0] for e in experiences]).to(self.device)
        actions = torch.vstack([e[1] for e in experiences]).to(self.device)
        rewards = torch.vstack([e[2] for e in experiences]).to(self.device)
        next_states = torch.vstack([e[3] for e in experiences]).to(self.device)
        dones = torch.vstack([e[4] for e in experiences]).to(self.device)
        return states, actions, rewards, next_states, dones

    def __len__(self):
        return len(self.memory)

