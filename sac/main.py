import getopt
import yaml
import sys
sys.path.append('../')
from sac.agent import SAC
from sys import platform
from mlagents_envs.environment import UnityEnvironment
from mlagents_envs.side_channel.engine_configuration_channel import EngineConfigurationChannel

# read command line arguments
args = sys.argv[2:]
optlist, _ = getopt.getopt(args, 'tt:gt:ot:lb')

opts = [t[0] for t in optlist]
test = '-t' in opts
ground = '-g' in opts
obstacle = '-o' in opts
leg = '-l' in opts
buffer = '-b' in opts

config_file = sys.argv[1]
with open(config_file) as f:
    config = yaml.safe_load(f)

# start crawler environment
engine_config_channel = EngineConfigurationChannel()
if ground and test:
    print("Starting crawler-environment with uneven ground...")
    if platform == "linux":
        env = UnityEnvironment("../envs/ground_uneven_linux/UnityEnvironment.x86_64", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)
    else:
        env = UnityEnvironment(r"..\envs\ground_uneven_win\UnityEnvironment.exe", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)

elif obstacle and test:
    print("Starting crawler-environment with obstacles...")
    if platform == "linux":
        env = UnityEnvironment("../envs/obstacles_static_linux/UnityEnvironment.x86_64", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)
    else:
        env = UnityEnvironment(r"..\envs\obstacles_static_win\UnityEnvironment.exe", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)

elif leg and test:
    print("Starting crawler-environment with one missing leg...")
    if platform == "linux":
        env = UnityEnvironment("../envs/crawler_missing_leg3_linux/UnityEnvironment.x86_64", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)
    else:
        env = UnityEnvironment(r"..\envs\crawler_missing_leg3_win\UnityEnvironment.exe", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)

else:
    print("Starting default crawler-environment...")
    if platform == "linux":
        env = UnityEnvironment("../envs/crawler_multi_linux_server/crawlerServer.x86_64", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)
    else:
        env = UnityEnvironment(r"..\envs\crawler_multi_win\UnityEnvironment.exe", worker_id=0,
                               side_channels=[engine_config_channel], seed=0)

#Set time-scale (speed) of agent
engine_config_channel.set_configuration_parameters(time_scale=config["time_scale"])
env.reset()

agent = SAC(env, config, test)
print("Device:", agent.device)
if not test and not buffer:
    print("Train configuration:")
    for param, val in config.items():
        print("     "+param+": ", val)
elif buffer:
    print("Creating buffer")
else:
    print("Testing...")

if buffer:
    # create buffer
    agent.create_random_buffer()
else:
    if not test:
        # Initialize buffer / Use this line to skip the warm-up phase and use a previously generated buffer
        agent.init_buffer(warmup=2000, path="random_buffer")
    # run train/test
    for i in range(config["max_steps"]):
        agent.step()

if agent.test:
    print("Average score over", agent.global_episode_count, "episodes:",
          sum(agent.test_scores) / len(agent.test_scores))
