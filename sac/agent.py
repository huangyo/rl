import torch
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import random
import pickle
import itertools

from typing import Dict
from collections import deque
from torch.utils.tensorboard import SummaryWriter

from sac.net import QNet, Actor
from sac.utils import ReplayBuffer


class SAC:

    def __init__(self, env, config, test=False, seed=None, log_dir=None):
        self.test = test
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        # Enviroment information
        self.env = env
        self.behavior_name = list(env.behavior_specs.keys())[0]
        self.behavior_spec = self.env.behavior_specs[self.behavior_name]
        act_size = self.behavior_spec.action_spec.continuous_size
        obs_size = self.behavior_spec.observation_shapes[0][0] + self.behavior_spec.observation_shapes[1][0]

        # Test or train
        if self.test:
            self.actor = Actor(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.load("../studies/v0.3")
        else:
            self.best_avg_score = -1

            # Set random seed
            if not seed:
                self.seed = random.randint(1, 10000)
                print("Random seed:", self.seed)
            else:
                self.seed = seed
                print("Set random seed to:", self.seed)
            torch.manual_seed(self.seed)
            np.random.seed(self.seed)

            # Hyperparameters
            self.config = config
            self.batch_size = config["batch_size"]
            self.buffer_size = config["buffer_size"]
            self.warm_up = config["warm_up"]  # number of random steps at the beginning of training
            self.update_every = config["update_every"]  # number of steps between two updates
            self.lr = config["lr"]
            self.gamma = config["gamma"]
            self.tau = config["tau"]
            self.auto = config["auto"]  # tune the temperature parameter alpha automatically

            # Epsilon-Decay-Parameters
            '''@author: Svenja König'''
            self.rbed = config["rbed"]
            if self.rbed:
                self.eps = config["eps_max"]
                self.eps_end = config["eps_min"]
                self.min_reward = config["r_threshold"]
                self.r_decay = (config["r_target"]) ** (
                        1 / float(config["decay_val"]))  # calc decay for reward depending on decay of epsilon
                self.min_reward_shadow = 1.0  # will be added to min_reward
                self.eps_delta = (self.eps - self.eps_end) / config["decay_val"]
                self.randomActions = 0
                self.actionLen = 0

            # Define networks and optimizers
            self.actor = Actor(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.q1 = QNet(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.q2 = QNet(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.target1 = QNet(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.target2 = QNet(obs_size, act_size, config["hid_dims"]).to(self.device)
            self.target1.load_state_dict(self.q1.state_dict())
            self.target2.load_state_dict(self.q2.state_dict())
            self.actor_optim = optim.Adam(self.actor.parameters(), lr=self.lr)
            self.q1_optim = optim.Adam(self.q1.parameters(), lr=self.lr)
            self.q2_optim = optim.Adam(self.q2.parameters(), lr=self.lr)
            if self.auto:
                self.target_entropy = -act_size  # recommended value from the original paper
                self.log_alpha = torch.tensor(-4.5, device=self.device, requires_grad=True)
                self.alpha = self.log_alpha.exp()
                self.alpha_optim = optim.Adam([self.log_alpha], lr=self.lr)
            else:
                self.alpha = config["alpha"]
            # Define replay buffer
            self.replay = ReplayBuffer(self.buffer_size, self.device, self.seed)

        # For logging
        self.global_step_count = 0  # count enviroment steps
        self.global_episode_count = 0  # count episodes of all agents
        self.last_obs_from_agent: Dict[int, np.ndarray] = {}
        self.last_action_from_agent: Dict[int, np.ndarray] = {}
        self.cumulative_reward_from_agent: Dict[int, float] = {}
        self.ep_len_from_agent: Dict[int, float] = {}
        if self.test:
            self.test_scores = []
        if not self.test:
            if not log_dir:
                self.writer = SummaryWriter()
            else:
                self.writer = SummaryWriter(log_dir=log_dir)
            self.scores = deque(maxlen=200)
            self.ep_lens = deque(maxlen=200)
            self.best_score = -1
            self.q1_loss = torch.zeros(1)
            self.q2_loss = torch.zeros(1)
            self.actor_loss = torch.zeros(1)

    def step(self):
        """
        @author: Dominik Beyer
        Steps the environment for each agent requesting a decision. During an episode, an agent requests decisions
        through decision_steps. An agent's episode has terminated when requesting a terminal step. When in testing
        mode, the environment is run without updating the model.
        """
        actions = []
        decision_steps, terminal_steps = self.env.get_steps(self.behavior_name)

        for agent_id in terminal_steps:
            cumulative_reward = (
                    self.cumulative_reward_from_agent.pop(agent_id)
                    + terminal_steps[agent_id].reward
            )
            episode_length = self.ep_len_from_agent.pop(agent_id)

            if not self.test:
                if agent_id in self.last_obs_from_agent:
                    observation = np.concatenate((terminal_steps[agent_id].obs[0], terminal_steps[agent_id].obs[1]), axis=0)
                    self.replay.add_experience(self.last_obs_from_agent.pop(agent_id),
                                               self.last_action_from_agent.pop(agent_id),
                                               terminal_steps[agent_id].reward,
                                               observation,
                                               not terminal_steps[agent_id].interrupted)
                    self.scores.append(cumulative_reward)
                    self.ep_lens.append(episode_length)
                    if self.rbed:
                        # Check if epsilon can be decayed
                        self.epsilon_decay(cumulative_reward)

                    if self.global_step_count > self.warm_up:
                        self.update()

                    # logging
                    self.log_all_agents(agent_id, cumulative_reward)
                    if self.rbed:
                        self.logging_random_actions()
                    if len(self.scores) >= 200 and self.global_episode_count % 100 == 0:
                        self.logging()

            else:
                print("Agent " + str(agent_id), "Score:", round(cumulative_reward, 4),
                      "Episode Length:", episode_length)  # print test result
                self.test_scores.append(cumulative_reward)

            self.global_episode_count += 1

        for agent_id in decision_steps:
            observation = np.concatenate((decision_steps[agent_id].obs[0], decision_steps[agent_id].obs[1]), axis=0)

            if agent_id not in self.cumulative_reward_from_agent:
                self.cumulative_reward_from_agent[agent_id] = 0
            if agent_id not in self.ep_len_from_agent:
                self.ep_len_from_agent[agent_id] = 0

            if not self.test:
                if agent_id in self.last_obs_from_agent:
                    self.replay.add_experience(self.last_obs_from_agent[agent_id],
                                               self.last_action_from_agent[agent_id],
                                               decision_steps[agent_id].reward,
                                               observation,
                                               False)
                self.last_obs_from_agent[agent_id] = observation
                if self.global_step_count > self.warm_up and self.global_step_count % self.update_every == 0:
                    self.update()

            self.ep_len_from_agent[agent_id] += 1
            self.cumulative_reward_from_agent[agent_id] += (
                decision_steps[agent_id].reward
            )

            state = torch.tensor(observation, dtype=torch.float).to(self.device)
            action = self.get_action(state)
            actions.append(action)
            self.last_action_from_agent[agent_id] = action

        if len(decision_steps) > 0:
            self.env.set_actions(self.behavior_name, np.array(actions))

        self.env.step()
        self.global_step_count += 1

        return

    def logging(self):
        """
        @author: Yongxin Huang
        Track average score and episode length over all agents over a time period. Save model if better score is achieved.
            [View Results]: tensorboard --logdir DIRNAME --port=6006
            [check if logs exits]: tensorboard --inspect --logdir DIRNAME
        """
        avg_ep_len = sum(self.ep_lens) / len(self.ep_lens)
        avg_score = sum(self.scores) / len(self.scores)
        best_score = max(max(self.scores), self.best_score)
        q1_loss, q2_loss, actor_loss = self.q1_loss.item(), self.q2_loss.item(), self.actor_loss.item()
        self.best_score = best_score
        self.writer.add_scalar("Score/Average", avg_score, self.global_step_count)
        self.writer.add_scalar("Score/Best", best_score, self.global_step_count)
        self.writer.add_scalar("Average episode length", avg_ep_len, self.global_step_count)
        self.writer.add_scalar("Loss/Q1", q1_loss, self.global_step_count)
        self.writer.add_scalar("Loss/Q2", q2_loss, self.global_step_count)
        self.writer.add_scalar("Loss/Actor", actor_loss, self.global_step_count)
        if self.auto:
            self.writer.add_scalar("Alpha", self.alpha.item(), self.global_step_count)
        print("Avg score:", round(avg_score, 4), "Best score:", round(best_score, 4),
              "Avg len:", round(avg_ep_len, 2), "Steps:", self.global_step_count,
              "q1:", round(q1_loss, 6), "q2:", round(q2_loss, 6), "actor:", round(actor_loss, 6))
        if avg_score > self.best_avg_score and avg_score > 1000:
            self.save()
            self.best_avg_score = avg_score

    def logging_random_actions(self):
        """
        @author: Svenja König
        Log random choosen actions and action choosen by exploition/policy in percent.
        """
        if self.actionLen > 0:
            self.writer.add_scalar("Actions/Random (%)", self.randomActions / (self.actionLen / 100),
                                   self.global_step_count)
            self.writer.add_scalar("Actions/Exploit (%)", 100 - (self.randomActions / (self.actionLen / 100)),
                                   self.global_step_count)
            self.randomActions = 0
            self.actionLen = 0

    def log_all_agents(self, agent_id, reward):
        """
        @author: Svenja König
        Log the cumulative reward of every singel agent for comparing.
        """
        self.writer.add_scalar("Agent-Scores/Agent" + str(agent_id), reward, self.global_step_count)

    def epsilon_decay(self, last_reward):
        """
        @author: Svenja König
        If no Warm-Up-Phase, current epsilon is bigger than min-epsilon and if the
        last reward is bigger than the min-reward, current epsilon will be reduced and min-reward increased.
        """

        if self.global_step_count > self.warm_up and self.eps >= self.eps_end and last_reward >= self.min_reward:
            self.eps -= self.eps_delta
            self.min_reward_shadow *= self.r_decay
            self.min_reward = self.min_reward_shadow - 2.0  # due to Difference of start-value of 'min-reward-shadow' and the reward at the bgeinning '-1.0'
            self.writer.add_scalar("Decay/Epsilon", self.eps, self.global_step_count)
            self.writer.add_scalar("Decay/Min-Reward", self.min_reward, self.global_step_count)
            print("Update epsilon....", "eps: ", self.eps, "min-reward:", self.min_reward)

    def get_action(self, state):
        """
        @author: Svenja König
        Get next action (by state).
        While exploring or Warm-Up-Phase a random-action will be choosen.
        Else Exploition.

        :param state: state to get next action
        :return: if warm-up or random number ist smaller than current epsilon,
        choose random action (Explore). Else Exploit.
        """
        if self.test:
            action = self.actor.test(state).cpu().detach().numpy()
            return action
        if self.rbed:
            # train mode
            self.actionLen += 1
            e_sample = np.random.uniform(self.config["eps_max"],
                                         self.config["eps_min"])  # get random number in range of max and min-epsilon
            if self.global_step_count < self.warm_up or e_sample <= self.eps:
                # Explore by taking a random action
                self.randomActions += 1
                action = self.behavior_spec.action_spec.random_action(n_agents=1)[0]
            else:
                # Exploit current knowledge about the system
                action = self.actor(state)[0].cpu().detach().numpy()
        else:
            if self.global_step_count < self.warm_up:
                action = self.behavior_spec.action_spec.random_action(n_agents=1)[0]
            else:
                action = self.actor(state)[0].cpu().detach().numpy()
        return action

    def critic_loss(self, states, actions, rewards, next_states, dones):
        """
        @author: Yongxin Huang
        Return loss of both Q-networks.
        """
        with torch.no_grad():
            next_actions, next_log_probs = self.actor(next_states)
            next_q_target1 = self.target1(next_states, next_actions)
            next_q_target2 = self.target2(next_states, next_actions)
            next_q_target = torch.min(next_q_target1, next_q_target2) - self.alpha * next_log_probs
            value = rewards + (1 - dones) * self.gamma * next_q_target

        q1 = self.q1(states, actions).to(self.device)
        q2 = self.q2(states, actions).to(self.device)
        q1_loss = F.mse_loss(q1, value).to(self.device)
        q2_loss = F.mse_loss(q2, value).to(self.device)

        return q1_loss, q2_loss

    def actor_and_alpha_loss(self, states):
        """
        @author: Yongxin Huang
        Return loss of policy network and alpha.
        """
        actions, log_probs = self.actor(states)
        q1 = self.q1(states, actions).to(self.device)
        q2 = self.q2(states, actions).to(self.device)
        q = torch.min(q1, q2)
        policy_loss = (self.alpha * log_probs - q).mean().to(self.device)
        if self.auto:
            alpha_loss = -(self.log_alpha * (log_probs + self.target_entropy).detach()).mean().to(self.device)
        else:
            alpha_loss = None
        return policy_loss, alpha_loss

    def update(self):
        """
        @author: Yongxin Huang
        Update Q-networks, policy network and target networks. Update alpha if auto-tune of alpha is True.
        """
        states, actions, rewards, next_states, dones = self.replay.sample(self.batch_size)
        self.q1_loss, self.q2_loss = self.critic_loss(states, actions, rewards, next_states, dones)
        # update Q nets
        self.q1_optim.zero_grad()
        self.q1_loss.backward()
        self.q1_optim.step()
        self.q2_optim.zero_grad()
        self.q2_loss.backward()
        self.q2_optim.step()
        # update actor and alpha
        self.actor_loss, alpha_loss = self.actor_and_alpha_loss(states)
        self.actor_optim.zero_grad()
        self.actor_loss.backward()
        self.actor_optim.step()
        if self.auto:
            self.alpha_optim.zero_grad()
            alpha_loss.backward()
            self.alpha_optim.step()
            self.alpha = self.log_alpha.exp()
        # update target nets
        for qnet, target in zip([self.q1, self.q2], [self.target1, self.target2]):
            params = dict()
            for key, value in qnet.state_dict().items():
                params[key] = self.tau * value + (1 - self.tau) * target.state_dict()[key]
            target.load_state_dict(params)
            target.eval()

    def create_random_buffer(self):
        """
        @author: Dominik Beyer
        Generates a file that contains random experience, which can be used for initializing the replay buffer.
        The number of experiences collected corresponds to the number of steps taken during warm-up phase.
        """
        for _ in range(self.warm_up):
            actions = []
            decision_steps, terminal_steps = self.env.get_steps(self.behavior_name)

            for agent_id in terminal_steps:
                if agent_id in self.last_obs_from_agent:
                    observation = np.concatenate((terminal_steps[agent_id].obs[0], terminal_steps[agent_id].obs[1]),
                                                 axis=0)
                    self.replay.add_experience(self.last_obs_from_agent.pop(agent_id),
                                               self.last_action_from_agent.pop(agent_id),
                                               terminal_steps[agent_id].reward,
                                               observation,
                                               not terminal_steps[agent_id].interrupted)

            for agent_id in decision_steps:
                observation = np.concatenate((decision_steps[agent_id].obs[0], decision_steps[agent_id].obs[1]), axis=0)

                if agent_id in self.last_obs_from_agent:
                    self.replay.add_experience(self.last_obs_from_agent[agent_id],
                                               self.last_action_from_agent[agent_id],
                                               decision_steps[agent_id].reward,
                                               observation,
                                               False)

                self.last_obs_from_agent[agent_id] = observation
                action = self.behavior_spec.action_spec.random_action(n_agents=1)[0]
                actions.append(action)
                self.last_action_from_agent[agent_id] = action

            if len(decision_steps) > 0:
                self.env.set_actions(self.behavior_name, np.array(actions))

            self.env.step()

        with open("random_buffer", "wb") as f:
            pickle.dump(self.replay.memory, f)

        print("Generated buffer of size {}".format(self.replay.__len__()))

        return

    def init_buffer(self, warmup, path):
        """
        @author: Dominik Beyer
        Initializes the replay buffer and skips the warm-up phase, if the specified file does exist and the specified
        warm-up size doesn't exceed the buffer size.

        :param warmup: The size with which the replay buffer should initially be filled.
        :param path: Path of the file to be used for initializing the replay buffer
        """
        print("Initializing buffer...")

        try:
            with open(path, "rb") as f:
                random_buffer = pickle.load(f)
        except FileNotFoundError:
            print("Buffer file doesn't exist. Training will be executed with warm-up instead.")
            return

        if warmup < len(random_buffer):
            self.warm_up = 0
            self.replay.memory = list(itertools.islice(random_buffer, 0, warmup))
            print("Initialized buffer of size {}".format(self.replay.__len__()))
        else:
            print(
                "The specified warm-up length exceeds the buffer size. Training will be executed with warm-up instead.")

        return

    def save(self, DIR_PATH='.'):
        """
        @author: Adrian Kruger
        Save the actor and critic models

        :param DIR_PATH: Path of the directory in which files shall be saved
        :return: True, if successful
        """

        print("Saving model to {}".format(str(DIR_PATH)))

        # torch.save(self.q1.state_dict(), DIR_PATH + "/q1.pt")
        # torch.save(self.q2.state_dict(), DIR_PATH + "/q2.pt")
        torch.save(self.actor.state_dict(), DIR_PATH + "/actor.pt")

        return True

    def load(self, DIR_PATH='.'):
        """
        @author: Adrian Kruger
        Load the actor and critic models (NOT the config file!)

        :param DIR_PATH: Path of the directory containing the saved models_01
        :return: True, if successful
        """

        print("Loading model from {}".format(str(DIR_PATH)))

        # self.q1.load_state_dict(torch.load(DIR_PATH + "/q1.pt", map_location=self.device))
        # self.q2.load_state_dict(torch.load(DIR_PATH + "/q2.pt", map_location=self.device))
        self.actor.load_state_dict(torch.load(DIR_PATH + "/actor.pt", map_location=self.device))

        return True
